'use strict';

var modernFETraining = {
	
	hintMsg: $('.hint'),

	init: function(){
		this.hideHintMsg();	
	},
	hideHintMsg: function(){
		
		var that = this;

		setTimeout(function(){
			that.hintMsg.fadeOut();	
		}, 2000);
	}
};

$(function(){
	modernFETraining.init();
});